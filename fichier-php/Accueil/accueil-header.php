<div id="header">
    <a href="index.php" class="logo">
        <img src="../images/logo.jpg" alt="">
    </a>
    <ul id="navigation">
        <li class="selected">
            <a href="index.php">home</a>
        </li>
        <li>
            <a href="about.php">about</a>
        </li>
        <li>
            <a href="gallery.php">gallery</a>
        </li>
        <li>
            <a href="blog.php">blog</a>
        </li>
        <li>
            <a href="contact.php">contact</a>
        </li>
    </ul>
</div>
